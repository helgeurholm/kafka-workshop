package net.scalytica.kafkaworkshop.streams

import java.util.concurrent.TimeUnit

import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.LazyLogging

import net.scalytica.kafkaworkshop.shared._
import net.scalytica.kafkaworkshop.shared.serdes.CirceSerde
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.streams.scala.ImplicitConversions._
import org.apache.kafka.streams.scala.Serdes._
import org.apache.kafka.streams.scala.StreamsBuilder
import org.apache.kafka.streams.{KafkaStreams, StreamsConfig}
import io.circe.generic.auto._

object StreamsApp extends App with LazyLogging {

  val proc = StreamProcessor.start()

  proc.start()

  sys.ShutdownHookThread {
    // scalastyle:off magic.number
    proc.close(10L, TimeUnit.SECONDS)
    // scalastyle:on magic.number
  }
}

object StreamProcessor extends LazyLogging {

  val cfg       = ConfigFactory.load().getConfig("kafkaworkshop.kafka")
  val kafkaUrl  = cfg.asString("url")("localhost:29092")
  val appId     = cfg.asString("applicationId")("workshop-stream-processor")
  val topicName = cfg.asString("topic")("workshop-input-topic")
  val aggregatedTopicName =
    cfg.asString("aggregatedTopic")("workshop-aggregated-topic")

  implicit val statsSerde    = new CirceSerde[Stats]
  implicit val statsSeqSerde = new CirceSerde[Seq[Stats]]
  implicit val aggrSerde     = new CirceSerde[AggregatedStats]

  def start(): KafkaStreams = {
    val streamCfg = Map(
      StreamsConfig.APPLICATION_ID_CONFIG     -> appId,
      StreamsConfig.BOOTSTRAP_SERVERS_CONFIG  -> kafkaUrl,
      StreamsConfig.COMMIT_INTERVAL_MS_CONFIG -> "10000",
      ConsumerConfig.AUTO_OFFSET_RESET_CONFIG -> "earliest"
    )

    val streamBuilder = new StreamsBuilder

    val baseStream = streamBuilder.stream[String, Stats](topicName)

    val aggrStream =
      streamBuilder.stream[String, AggregatedStats](aggregatedTopicName)

    aggrStream.foreach { (key, value) =>
      logger.info(
        s"""Consuming aggregated stream
           |   key: $key
           |   value: $value
           """.stripMargin
      )
    }

    baseStream.foreach { (key, value) =>
      logger.debug(s"Processing record with key=$key and value=$value")
    }

    baseStream
      .peek((k, _) => logger.debug(s"Sending key=$k to aggregation pipeline"))
      .groupByKey
      .aggregate(Seq.empty[Stats]) {
        case (k, value, aggregated) =>
          logger.debug(s"$k got appended: $value")
          aggregated :+ value
      }
      .filterNot((_, v) => v.isEmpty)
      .mapValues { value =>
        AggregatedStats(
          region = value.head.region,
          yearlyStats = value.map { s =>
            HouseholdStats(s.household, s.year, s.count)
          }
        )
      }
      .toStream
      .to(aggregatedTopicName)

    new KafkaStreams(streamBuilder.build(), streamCfg)
  }

}
