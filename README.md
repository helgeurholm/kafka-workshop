# Kafka Workshop Demo

Demo application used in Kafka Workshop to show simple uses of producers, consumeres and stream processing.

Producer and consumer are both Using akka-streams for data processing. While the stream processing app uses the kafka-streams API.
