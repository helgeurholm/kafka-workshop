package net.scalytica.kafkaworkshop.consumer

import akka.actor.ActorSystem
import akka.kafka.scaladsl.Consumer
import akka.kafka.{ConsumerSettings, Subscriptions}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Keep, Sink}
import akka.{Done, NotUsed}
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.LazyLogging
import io.circe.generic.auto._
import net.scalytica.kafkaworkshop.shared._
import net.scalytica.kafkaworkshop.shared.serdes.CirceDeserializer
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.Serdes

import scala.concurrent.Future

object ConsumerApp extends App with LazyLogging {

  implicit val as  = ActorSystem("consumer-app-system")
  implicit val mat = ActorMaterializer()
  implicit val ec  = as.dispatcher

  logger.info("Starting consumer...")

  val consumer = new DataConsumer()

  val (consumerCtrl, streamCompleted) = consumer.start

  streamCompleted.onComplete { _ =>
    logger.info("Consumer completed reading data from kafka.")
    consumerCtrl.shutdown()
    logger.info("Terminating actor system...")
    mat.shutdown()
    as.terminate()
    logger.info("Actor system terminated. Stopping application")
  }

}

class DataConsumer()(implicit as: ActorSystem, mat: ActorMaterializer)
    extends LazyLogging {

  val cfg       = ConfigFactory.load().getConfig("kafkaworkshop.kafka")
  val kafkaUrl  = cfg.asString("url")("localhost:29092")
  val clientId  = cfg.asString("clientId")("workshop-consumer")
  val topicName = cfg.asString("topic")("workshop-input-topic")
  val groupId   = cfg.asString("groupId")("workshop-consumer-group")

  private[this] val consumerSettings =
    ConsumerSettings(
      as,
      Serdes.String().deserializer(),
      new CirceDeserializer[Stats]
    ).withBootstrapServers(kafkaUrl)
      .withGroupId(groupId)
      .withProperties(
        ConsumerConfig.CLIENT_ID_CONFIG         -> clientId,
        ConsumerConfig.AUTO_OFFSET_RESET_CONFIG -> "earliest"
      )

  private[this] val subscription = Subscriptions.topics(topicName)

  def start: (Consumer.Control, Future[Done]) =
    Consumer
//      .committableSource(consumerSettings, subscription)
//      .map(_.record.value())
      .plainSource(consumerSettings, subscription)
      .map(_.value())
      .via(logStats)
      .toMat(Sink.ignore)(Keep.both)
      .run()

  private[this] def logStats: Flow[Stats, Stats, NotUsed] =
    Flow[Stats].map { s =>
      logger.debug(s"Consumed kafka record: $s")
      s
    }

}
