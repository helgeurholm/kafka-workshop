#!/usr/bin/env bash

echo "   __    __           _        _                           ";
echo "  / / /\ \ \___  _ __| | _____| |__   ___  _ __            ";
echo "  \ \/  \/ / _ \| '__| |/ / __| '_ \ / _ \| '_ \           ";
echo "   \  /\  / (_) | |  |   <\__ \ | | | (_) | |_) |          ";
echo "    \/  \/ \___/|_|  |_|\_\___/_| |_|\___/| .__/           ";
echo "                                          |_|              ";
echo "   __           _                                      _   ";
echo "  /__\ ____   _(_)_ __ ___  _ __  _ __ ___   ___ _ __ | |_ ";
echo " /_\| '_ \ \ / / | '__/ _ \| '_ \| '_ \` _ \ / _ \ '_ \| __|";
echo "//__| | | \ V /| | | | (_) | | | | | | | | |  __/ | | | |_ ";
echo "\__/|_| |_|\_/ |_|_|  \___/|_| |_|_| |_| |_|\___|_| |_|\__|";
echo "                                                           ";

CMD=$1
FLAG=$2

CURR_DIR=$(pwd)
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
ROOT_DIR=$( cd $SCRIPT_DIR/../..; pwd )

if [[ "$(uname)" == "Darwin" ]]; then
  export HOST_IP=$(ipconfig getifaddr en0)
elif [[ "$(uname)" == "Linux" ]]; then
  export HOST_IP=$(ip route get 1 | awk '{print $NF;exit}')
else
  echo "Script does not currently support Windows. The alternative option is:"
  echo "  1. Manually export and set the 'HOST_IP' environment variable."
  echo "  2. Make sure you are standing in the correct directory (same dir as docker-compose.yml file)"
  echo "  3. run: docker-compose up -d --build --force-recreate"
  echo "  4. stop: docker-compose down --remove-orphans --rmi"
  exit 1
fi

# Start containers defined in docker-compose file
function start() {
  echo "Starting docker-compose build..."
  docker-compose up -d --build --force-recreate

  echo "Initializing topics..."
  sleep 15;

  docker exec -it cluster_kafka1_1 kafka-topics \
    --zookeeper zookeeper1:22181 \
    --create \
    --topic workshop-input-topic \
    --partitions 1 \
    --replication-factor 1

  docker exec -it cluster_kafka1_1 kafka-topics \
    --zookeeper zookeeper1:22181 \
    --create \
    --topic workshop-aggregated-topic \
    --partitions 1 \
    --replication-factor 1

  echo "Kafka cluster is ready!"
}

# Take down containers and remove any containers build by docker-compose
function stop() {
  echo "Stopping services..."
  docker-compose down --remove-orphans --rmi local
}

cd $SCRIPT_DIR

if [ "$CMD" = "start" ]; then
  start

elif [ "$CMD" = "stop" ]; then
  stop

elif [ "$CMD" = "restart" ]; then
  stop
  start

else
  echo "Usage: $ backends.sh <start | stop | restart>"
fi

cd $CURR_DIR