#! /bin/bash

docker exec -it single-node_kafka_1 kafka-streams-application-reset --application-id workshop-stream-processor \
                                      --input-topics workshop-input-topic workshop-aggregated-topic \
                                      --intermediate-topics workshop-aggregated-topic \
                                      --bootstrap-servers localhost:29092 \
                                      --zookeeper zookeeper:222181

rm -rf /tmp/kafka-streams

